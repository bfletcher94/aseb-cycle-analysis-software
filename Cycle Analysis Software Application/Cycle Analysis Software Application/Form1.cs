﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace Cycle_Analysis_Software_Application
{
    public partial class Form1 : Form
    {
        List<string> cycleData = new List<string>(); //list of lines from the body of the file
        //int lists for each seperate column of cycle data
        List<int> hr_list = new List<int>();
        List<int> speed_list = new List<int>();
        List<int> power_list = new List<int>();
        List<int> altitude_list = new List<int>();
        List<int> cadence_list = new List<int>();
        //variable to be used to calculate the distance
        double timelengthinhours;
        //graph curves
        private LineItem heartRateLine;
        private LineItem speedLine;
        private LineItem cadenceLine;
        private LineItem altitudeLine;
        private LineItem powerLine; 

        private string filePath = @"C:\Users\Ben Fletcher\Documents\Cycle Analysis Software Application\Cycle Analysis Software Application\ASDBExampleCycleComputerData.hrm";

        public Form1()
        {
            InitializeComponent();

            string filePath = @"C:\Users\Ben Fletcher\Documents\Cycle Analysis Software Application\Cycle Analysis Software Application\ASDBExampleCycleComputerData.hrm";

            if (File.Exists(filePath))
            {
                getDate(filePath);
                getStartTime(filePath);
                getLength(filePath);
                getInterval(filePath);
                getMaxHR(filePath);
                getRestHR(filePath);
                getVO2(filePath);
                getWeight(filePath);
            }
            else
                MessageBox.Show("Error - File {0}  not Found", filePath);
        }

        private void ReadBody()
        {

            int linecount = 0; //line count initially zero

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                string oneLine;

                while ((oneLine = myfileReader.ReadLine()) != null)
                {
                    //read from line 130 when the cycle data begins
                    if (linecount > 130)
                    {
                        //add each line to the list
                        cycleData.Add(oneLine);
                    }

                    linecount++;
                }

                myfileReader.Close();

            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

        }

        //Populates datagridview
        private void PopulateDataGrid()
        {
            string dateString = getDate(filePath); //getting date string from getDate method
            DateTime date = DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture); //parsing the date

            string startTimeString = getStartTime(filePath); //getting time as a string from the getStartTime method
            DateTime startTime = DateTime.ParseExact(startTimeString, "HH:mm:ss.FFF", CultureInfo.InvariantCulture);

            startTime = startTime.AddSeconds(-1);
            
            for (int x = 0; x < cycleData.Count; x++)
            {
                //add 1 second onto the time each time it loops
                startTime = startTime.AddSeconds(1);

                //concatenates the date and time onto the first two rows of the datagridview
                cycleData[x] = date.ToShortDateString() + '	' + startTime.ToLongTimeString() + '	' + cycleData[x];

                //Splits the data where theres a space and adds to the datagridview object
                CycleDataGrid.Rows.Add(cycleData[x].Split('	'));
            }

        }

        private string getDate(String filePath)
        {
            string oneline;
            

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("Date"))
                    {
                        string[] words = oneline.Split('=');
                        
                        string dateString = words[1]; //1st index, after the equals sign is the date string
                        DateTime date = DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture); //parsing the date
                        lblDate.Text = date.ToShortDateString(); //short date format
                        return words[1];
                         
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        private string getStartTime(String filePath)
        {
            string oneline;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("StartTime"))
                    {
                        
                        string[] words = oneline.Split('=');
                         
                        lblStartTime.Text = words[1]; //displays the value in correct label on the form

                        return words[1];               
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        private void getLength(String filePath)
        {
            string oneline;
            int lineNo = 1;
            
            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("Length"))
                    {
                        string[] words = oneline.Split('=');
                        for (int x = 0; x < words.Length; x++)
                            
                            lblLength.Text = words[x];
                        
                        lineNo++;
                        //splitting the length value into substrings so can handle the hours minutes and seconds seperately.
                        string length = oneline.Substring(oneline.Length - 10);
                        string hoursString = length.Substring(0, 2);
                        string minutesString = length.Substring(3, 2);
                        string secondsString = length.Substring(6, 2);
                        int hours = Int32.Parse(hoursString);
                        int minutes = Int32.Parse(minutesString);
                        int seconds = Int32.Parse(secondsString);

                        //time in seconds using the substring values
                        double timelengthinseconds = (hours * 3600) + (minutes * 60) + seconds;
                        //time in hours to be used to later calculate the total distance
                        timelengthinhours = timelengthinseconds / 3600;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getInterval(String filePath)
        {
            string oneline;
            int lineNo = 1;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("Interval"))
                    {
                        
                        string[] words = oneline.Split('=');
                        for (int x = 0; x < words.Length; x++)
                            
                            lblInterval.Text = words[x] + " second";

                        lineNo++;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getMaxHR(String filePath)
        {
            string oneline;
            int lineNo = 1;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("MaxHR"))
                    {
                        
                        string[] words = oneline.Split('=');
                        for (int x = 0; x < words.Length; x++)
                            
                            lblMaxHR.Text = words[x] + " bpm";

                        lineNo++;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getRestHR(String filePath)
        {
            string oneline;
            int lineNo = 1;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("RestHR"))
                    {
                        string[] words = oneline.Split('=');
                        for (int x = 0; x < words.Length; x++)
                            
                            lblRestHR.Text = words[x] + " bpm";

                        lineNo++;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getVO2(String filePath)
        {
            string oneline;
            int lineNo = 1;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("VO2max"))
                    {
                        
                        string[] words = oneline.Split('=');
                        for (int x = 0; x < words.Length; x++)
                            
                            lblVO2.Text = words[x];

                        lineNo++;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getWeight(String filePath)
        {
            string oneline;
            int lineNo = 1;

            try
            {
                StreamReader myfileReader = new StreamReader(filePath);

                while ((oneline = myfileReader.ReadLine()) != null)
                {
                    if (oneline.Contains("Weight"))
                    {
                        string[] words = oneline.Split('='); //gets the weight value from text file after the equals sign.
                        for (int x = 0; x < words.Length; x++)
                            
                            lblWeight.Text = words[x] + " kg";
                        
                        lineNo++;
                    }
                }

                myfileReader.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        private void getColumnData()
        {
            foreach (DataGridViewRow item in CycleDataGrid.Rows)
            { 
                //adds heart rate values to the heart rate list
                if(item.Cells[2].Value!= null)
                {
                   hr_list.Add(Int32.Parse(item.Cells[2].Value.ToString()));
                }

                //adds speed values to the speed list
                if (item.Cells[3].Value != null)
                {
                   speed_list.Add(Int32.Parse(item.Cells[3].Value.ToString()));
                }

                //adds cadence values to cadence list
                if (item.Cells[4].Value != null)
                {
                    cadence_list.Add(Int32.Parse(item.Cells[4].Value.ToString()));
                }
                
                //adds altitude values to altitude list
                if (item.Cells[5].Value != null)
                {
                   altitude_list.Add(Int32.Parse(item.Cells[5].Value.ToString()));
                }

                //adds power values to the power list
                if (item.Cells[6].Value != null)
                {
                   power_list.Add(Int32.Parse(item.Cells[6].Value.ToString()));
                }
            }
        }

        private void setSummaryData()
        {
            
            //average speed
            double avspeed = (speed_list.Sum()/speed_list.Count()) * 0.1;
            lblAvSpeed.Text = avspeed + " km/h";

            //total distance
            double distance = timelengthinhours * avspeed;
            distance = Math.Round(distance, 2);
            lblTotalDist.Text = distance + " km";

            //max speed
            lblMaxSpeed.Text = speed_list.Max() * 0.1 + " km/h";

            //average HR
            lblAvHR.Text = hr_list.Sum()/hr_list.Count() + " bpm";

            //max HR
            lblMaxHeartRate.Text = hr_list.Max() + " bpm";

            //min HR
            lblMinHR.Text = hr_list.Min() + " bpm";

            //average power
            lblAvPower.Text = power_list.Sum() / power_list.Count() + " W";

            //max power
            lblMaxPower.Text = power_list.Max() + " W";

            //average altitude
            lblAvAltitude.Text = altitude_list.Sum() / altitude_list.Count() + " m";

            //maximum altitude
            lblMaxAltitude.Text = altitude_list.Max() + " m";
        }

        

        //radio button events for conversion between km/h and mph
        private void radioKm_CheckedChanged(object sender, EventArgs e)
        {
            //average speed
            double avspeed = (speed_list.Sum() / speed_list.Count()) * 0.1;
            lblAvSpeed.Text = avspeed + " km/h";

            //total distance
            double distance = timelengthinhours * avspeed;
            distance = Math.Round(distance, 2);
            lblTotalDist.Text = distance + " km";

            //max speed
            lblMaxSpeed.Text = speed_list.Max() * 0.1 + " km/h";
        }

        private void radioMph_CheckedChanged(object sender, EventArgs e)
        {
            //average speed
            double avspeed = (speed_list.Sum() / speed_list.Count()) * 0.1;
            double avspeedmiles = avspeed / 1.6;
            avspeedmiles = Math.Round(avspeedmiles, 2); //convert and round to 2 decimal places
            lblAvSpeed.Text = avspeedmiles + " mph";
             
            //total distance
            double distance = timelengthinhours * avspeed;
            double distancemiles = distance / 1.6;
            distancemiles = Math.Round(distancemiles, 2);
            lblTotalDist.Text = distancemiles + " miles";

            //max speed
            double speedmph = (speed_list.Max() * 0.1) / 1.6;
            speedmph = Math.Round(speedmph, 2);
            lblMaxSpeed.Text = speedmph + " mph";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReadBody();
            PopulateDataGrid();
            getColumnData();
            setSummaryData();
            SetSize(); //graph related method
            CreateGraph(zedGraphControl1);// Setup the graph
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            SetSize(); //graph related method
        }

        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 30);
            // Leave a small margin around the outside of the control to fit in the graph tab page
            zedGraphControl1.Size = new Size(tabPageGraph.Width - 25,
                                    tabPageGraph.Height - 40);
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;

            //2nd y axis for the speed and altitude
            Y2Axis yspeed = new Y2Axis("Speed");
            Y2Axis yaltitude = new Y2Axis("Altitude");
            yaltitude.IsVisible = true; //altitude axis visible
            myPane.Y2AxisList.Add(yaltitude); //add altitude to y2 axis list
            myPane.Y2Axis.IsVisible = true; //enables visibility of 2nd y axis
            
            //other y axis for power
            YAxis ypower = new YAxis("Power (W)");
            myPane.YAxisList.Add(ypower);

            //other y axis for cadence
            YAxis ycadence = new YAxis("Cadence");
            myPane.YAxisList.Add(ycadence);

            // Set the Titles
            myPane.Title.Text = "Cycle Data Graph";
            myPane.XAxis.Title.Text = "Time (HH:MM:SS)";
            myPane.YAxis.Title.Text = "Heart Rate (bpm)";
            myPane.Y2Axis.Title.Text = "Speed (km/h)";

            //Scale for time on the X axis
            myPane.XAxis.Type = AxisType.Date;
            myPane.XAxis.Scale.Format = "HH:mm:ss";
            myPane.XAxis.Scale.MajorUnit = DateUnit.Second; //scales up by the Second

            //Parse date and time for x axis
            string dateTime = getDate(filePath)+" "+getStartTime(filePath);
            DateTime dateTimeScale = DateTime.ParseExact(dateTime, "yyyyMMdd HH:mm:ss.FFF", CultureInfo.InvariantCulture);

            // Move the legend location
            myPane.Legend.Position = ZedGraph.LegendPos.Bottom;  
            
            PointPairList hrPointList = new PointPairList();
            PointPairList speedPointList = new PointPairList();
            PointPairList powerPointList = new PointPairList();
            PointPairList altitudePointList = new PointPairList();
            PointPairList cadencePointList = new PointPairList();

            //PLOT HEART RATE POINTS
            for (int i = 0; i < hr_list.Count; i = i + 1)
            {
                double heartrateY = hr_list[i];
                hrPointList.Add(new XDate(dateTimeScale.AddSeconds(i)), heartrateY); //adds the time and date scale and adds a second each time
            }
            //draw the curve
            heartRateLine = myPane.AddCurve("Heart Rate", hrPointList, Color.Red, SymbolType.None);

            //PLOT SPEED POINTS
            for (int i = 0; i < speed_list.Count; i = i + 1)
            {
                double speedY = speed_list[i]*0.1;
                speedPointList.Add(new XDate(dateTimeScale.AddSeconds(i)), speedY); //adds the time and date scale and adds a second each time
            }
            //draw curve
            speedLine = myPane.AddCurve("Speed", speedPointList, Color.Blue, SymbolType.None);
            speedLine.IsY2Axis = true; //assigns curve to the y2 speed axis.

            //PLOT POINTS FOR POWER
            for (int i = 0; i < power_list.Count; i = i + 1)
            {
                double powerY = power_list[i];
                powerPointList.Add(new XDate(dateTimeScale.AddSeconds(i)), powerY); //adds the time and date scale and adds a second each time
            }
            //draw the curve
            powerLine = myPane.AddCurve("Power", powerPointList, Color.Purple, SymbolType.None);
            powerLine.YAxisIndex = 1; //associates curve with the second y axis.

            //PLOT POINTS FOR CADENCE
            for (int i = 0; i < cadence_list.Count; i = i + 1)
            { 
                double cadenceY = cadence_list[i];
                cadencePointList.Add(new XDate(dateTimeScale.AddSeconds(i)), cadenceY); //adds the time and date scale and adds a second each time
            }
            
            cadenceLine = myPane.AddCurve("Cadence", cadencePointList, Color.Brown, SymbolType.None);
            cadenceLine.YAxisIndex = 2; //associates curve with the third y axis.

            //PLOT POINTS FOR ALTITUDE
            for (int i = 0; i < altitude_list.Count; i = i + 1)
            {
                double altitudeY = altitude_list[i];
                altitudePointList.Add(new XDate(dateTimeScale.AddSeconds(i)), altitudeY); //adds the time and date scale and adds a second each time
            }

            altitudeLine = myPane.AddCurve("Altitude", altitudePointList, Color.Green, SymbolType.None);
            altitudeLine.IsY2Axis = true; //associates curve with y2 axis.
            altitudeLine.YAxisIndex = 1; //associates curve with the second y2 axis.

            zgc.AxisChange();

            zedGraphControl1.Refresh();
        }

        private void checkBoxHR_CheckedChanged(object sender, EventArgs e)
        {
            //for the check boxes
            if (checkBoxHR.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(heartRateLine);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(heartRateLine);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBoxSpeed_CheckedChanged(object sender, EventArgs e)
        {
            //for the check boxes
            if (checkBoxSpeed.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(speedLine);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(speedLine);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBoxCadence_CheckedChanged(object sender, EventArgs e)
        {
            //for the check boxes
            if (checkBoxCadence.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(cadenceLine);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(cadenceLine);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBoxAltitude_CheckedChanged(object sender, EventArgs e)
        {
            //for the check boxes
            if (checkBoxAltitude.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(altitudeLine);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(altitudeLine);
            }

            zedGraphControl1.Refresh();
        }

        private void checkBoxPower_CheckedChanged(object sender, EventArgs e)
        {
            //for the check boxes
            if (checkBoxPower.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(powerLine);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(powerLine);
            }

            zedGraphControl1.Refresh();
        }
    }
}
