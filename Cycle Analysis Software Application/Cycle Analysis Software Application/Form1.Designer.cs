﻿namespace Cycle_Analysis_Software_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CycleDataGrid = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblLength = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMaxHR = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblRestHR = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblVO2 = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblTotalDist = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblAvSpeed = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblMaxSpeed = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblAvHR = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblMaxHeartRate = new System.Windows.Forms.Label();
            this.lblMinHR = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblAvPower = new System.Windows.Forms.Label();
            this.lblMaxPower = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblAvAltitude = new System.Windows.Forms.Label();
            this.lblMaxAltitude = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPageGraph = new System.Windows.Forms.TabPage();
            this.checkBoxPower = new System.Windows.Forms.CheckBox();
            this.checkBoxAltitude = new System.Windows.Forms.CheckBox();
            this.checkBoxCadence = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxHR = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.radioKm = new System.Windows.Forms.RadioButton();
            this.radioMiles = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.CycleDataGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPageGraph.SuspendLayout();
            this.SuspendLayout();
            // 
            // CycleDataGrid
            // 
            this.CycleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CycleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Time,
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.Power});
            this.CycleDataGrid.Location = new System.Drawing.Point(-11, 6);
            this.CycleDataGrid.Name = "CycleDataGrid";
            this.CycleDataGrid.Size = new System.Drawing.Size(744, 264);
            this.CycleDataGrid.TabIndex = 0;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            // 
            // Time
            // 
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rate (bpm)";
            this.HeartRate.Name = "HeartRate";
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed (km/h)";
            this.Speed.Name = "Speed";
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence";
            this.Cadence.Name = "Cadence";
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude";
            this.Altitude.Name = "Altitude";
            // 
            // Power
            // 
            this.Power.HeaderText = "Power (W)";
            this.Power.Name = "Power";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(91, 123);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(35, 13);
            this.lblLength.TabIndex = 1;
            this.lblLength.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Length: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Start Time:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(91, 76);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(35, 13);
            this.lblDate.TabIndex = 5;
            this.lblDate.Text = "label4";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Location = new System.Drawing.Point(91, 99);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(35, 13);
            this.lblStartTime.TabIndex = 6;
            this.lblStartTime.Text = "label4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Interval:";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(225, 76);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(35, 13);
            this.lblInterval.TabIndex = 8;
            this.lblInterval.Text = "label5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(173, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Max HR:";
            // 
            // lblMaxHR
            // 
            this.lblMaxHR.AutoSize = true;
            this.lblMaxHR.Location = new System.Drawing.Point(225, 99);
            this.lblMaxHR.Name = "lblMaxHR";
            this.lblMaxHR.Size = new System.Drawing.Size(35, 13);
            this.lblMaxHR.TabIndex = 10;
            this.lblMaxHR.Text = "label6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(173, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Rest HR:";
            // 
            // lblRestHR
            // 
            this.lblRestHR.AutoSize = true;
            this.lblRestHR.Location = new System.Drawing.Point(225, 123);
            this.lblRestHR.Name = "lblRestHR";
            this.lblRestHR.Size = new System.Drawing.Size(35, 13);
            this.lblRestHR.TabIndex = 12;
            this.lblRestHR.Text = "label7";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(313, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "VO2 Max:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Weight:";
            // 
            // lblVO2
            // 
            this.lblVO2.AutoSize = true;
            this.lblVO2.Location = new System.Drawing.Point(371, 76);
            this.lblVO2.Name = "lblVO2";
            this.lblVO2.Size = new System.Drawing.Size(35, 13);
            this.lblVO2.TabIndex = 15;
            this.lblVO2.Text = "label9";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(371, 99);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(35, 13);
            this.lblWeight.TabIndex = 16;
            this.lblWeight.Text = "label9";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(210, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(381, 37);
            this.label9.TabIndex = 17;
            this.label9.Text = "Cycle Analysis Software";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 491);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Total Distance:";
            // 
            // lblTotalDist
            // 
            this.lblTotalDist.AutoSize = true;
            this.lblTotalDist.Location = new System.Drawing.Point(113, 491);
            this.lblTotalDist.Name = "lblTotalDist";
            this.lblTotalDist.Size = new System.Drawing.Size(41, 13);
            this.lblTotalDist.TabIndex = 19;
            this.lblTotalDist.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 514);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Average Speed:";
            // 
            // lblAvSpeed
            // 
            this.lblAvSpeed.AutoSize = true;
            this.lblAvSpeed.Location = new System.Drawing.Point(113, 514);
            this.lblAvSpeed.Name = "lblAvSpeed";
            this.lblAvSpeed.Size = new System.Drawing.Size(41, 13);
            this.lblAvSpeed.TabIndex = 21;
            this.lblAvSpeed.Text = "label13";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(179, 491);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Max Speed:";
            // 
            // lblMaxSpeed
            // 
            this.lblMaxSpeed.AutoSize = true;
            this.lblMaxSpeed.Location = new System.Drawing.Point(249, 491);
            this.lblMaxSpeed.Name = "lblMaxSpeed";
            this.lblMaxSpeed.Size = new System.Drawing.Size(41, 13);
            this.lblMaxSpeed.TabIndex = 23;
            this.lblMaxSpeed.Text = "label15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(179, 514);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "Average HR:";
            // 
            // lblAvHR
            // 
            this.lblAvHR.AutoSize = true;
            this.lblAvHR.Location = new System.Drawing.Point(249, 514);
            this.lblAvHR.Name = "lblAvHR";
            this.lblAvHR.Size = new System.Drawing.Size(41, 13);
            this.lblAvHR.TabIndex = 25;
            this.lblAvHR.Text = "label17";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(316, 491);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Max HR:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(316, 514);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "Min HR:";
            // 
            // lblMaxHeartRate
            // 
            this.lblMaxHeartRate.AutoSize = true;
            this.lblMaxHeartRate.Location = new System.Drawing.Point(366, 491);
            this.lblMaxHeartRate.Name = "lblMaxHeartRate";
            this.lblMaxHeartRate.Size = new System.Drawing.Size(41, 13);
            this.lblMaxHeartRate.TabIndex = 28;
            this.lblMaxHeartRate.Text = "label20";
            // 
            // lblMinHR
            // 
            this.lblMinHR.AutoSize = true;
            this.lblMinHR.Location = new System.Drawing.Point(366, 514);
            this.lblMinHR.Name = "lblMinHR";
            this.lblMinHR.Size = new System.Drawing.Size(41, 13);
            this.lblMinHR.TabIndex = 29;
            this.lblMinHR.Text = "label21";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(435, 491);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "Average Power:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(435, 514);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 13);
            this.label23.TabIndex = 31;
            this.label23.Text = "Max Power:";
            // 
            // lblAvPower
            // 
            this.lblAvPower.AutoSize = true;
            this.lblAvPower.Location = new System.Drawing.Point(517, 491);
            this.lblAvPower.Name = "lblAvPower";
            this.lblAvPower.Size = new System.Drawing.Size(41, 13);
            this.lblAvPower.TabIndex = 32;
            this.lblAvPower.Text = "label24";
            // 
            // lblMaxPower
            // 
            this.lblMaxPower.AutoSize = true;
            this.lblMaxPower.Location = new System.Drawing.Point(517, 514);
            this.lblMaxPower.Name = "lblMaxPower";
            this.lblMaxPower.Size = new System.Drawing.Size(41, 13);
            this.lblMaxPower.TabIndex = 33;
            this.lblMaxPower.Text = "label25";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(597, 491);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 13);
            this.label26.TabIndex = 34;
            this.label26.Text = "Average Altitude:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(597, 514);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 35;
            this.label27.Text = "Maximum Altitude:";
            // 
            // lblAvAltitude
            // 
            this.lblAvAltitude.AutoSize = true;
            this.lblAvAltitude.Location = new System.Drawing.Point(688, 491);
            this.lblAvAltitude.Name = "lblAvAltitude";
            this.lblAvAltitude.Size = new System.Drawing.Size(41, 13);
            this.lblAvAltitude.TabIndex = 36;
            this.lblAvAltitude.Text = "label28";
            // 
            // lblMaxAltitude
            // 
            this.lblMaxAltitude.AutoSize = true;
            this.lblMaxAltitude.Location = new System.Drawing.Point(689, 514);
            this.lblMaxAltitude.Name = "lblMaxAltitude";
            this.lblMaxAltitude.Size = new System.Drawing.Size(41, 13);
            this.lblMaxAltitude.TabIndex = 37;
            this.lblMaxAltitude.Text = "label29";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPageGraph);
            this.tabControl1.Location = new System.Drawing.Point(30, 154);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(744, 320);
            this.tabControl1.TabIndex = 38;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.CycleDataGrid);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(736, 294);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Table";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPageGraph
            // 
            this.tabPageGraph.AutoScroll = true;
            this.tabPageGraph.Controls.Add(this.checkBoxPower);
            this.tabPageGraph.Controls.Add(this.checkBoxAltitude);
            this.tabPageGraph.Controls.Add(this.checkBoxCadence);
            this.tabPageGraph.Controls.Add(this.checkBoxSpeed);
            this.tabPageGraph.Controls.Add(this.checkBoxHR);
            this.tabPageGraph.Controls.Add(this.label11);
            this.tabPageGraph.Controls.Add(this.zedGraphControl1);
            this.tabPageGraph.Location = new System.Drawing.Point(4, 22);
            this.tabPageGraph.Name = "tabPageGraph";
            this.tabPageGraph.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGraph.Size = new System.Drawing.Size(736, 294);
            this.tabPageGraph.TabIndex = 1;
            this.tabPageGraph.Text = "Graph";
            this.tabPageGraph.UseVisualStyleBackColor = true;
            // 
            // checkBoxPower
            // 
            this.checkBoxPower.AutoSize = true;
            this.checkBoxPower.Location = new System.Drawing.Point(359, 12);
            this.checkBoxPower.Name = "checkBoxPower";
            this.checkBoxPower.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPower.TabIndex = 49;
            this.checkBoxPower.Text = "Power";
            this.checkBoxPower.UseVisualStyleBackColor = true;
            this.checkBoxPower.CheckedChanged += new System.EventHandler(this.checkBoxPower_CheckedChanged);
            // 
            // checkBoxAltitude
            // 
            this.checkBoxAltitude.AutoSize = true;
            this.checkBoxAltitude.Location = new System.Drawing.Point(285, 12);
            this.checkBoxAltitude.Name = "checkBoxAltitude";
            this.checkBoxAltitude.Size = new System.Drawing.Size(61, 17);
            this.checkBoxAltitude.TabIndex = 48;
            this.checkBoxAltitude.Text = "Altitude";
            this.checkBoxAltitude.UseVisualStyleBackColor = true;
            this.checkBoxAltitude.CheckedChanged += new System.EventHandler(this.checkBoxAltitude_CheckedChanged);
            // 
            // checkBoxCadence
            // 
            this.checkBoxCadence.AutoSize = true;
            this.checkBoxCadence.Location = new System.Drawing.Point(205, 12);
            this.checkBoxCadence.Name = "checkBoxCadence";
            this.checkBoxCadence.Size = new System.Drawing.Size(69, 17);
            this.checkBoxCadence.TabIndex = 47;
            this.checkBoxCadence.Text = "Cadence";
            this.checkBoxCadence.UseVisualStyleBackColor = true;
            this.checkBoxCadence.CheckedChanged += new System.EventHandler(this.checkBoxCadence_CheckedChanged);
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Location = new System.Drawing.Point(135, 12);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(57, 17);
            this.checkBoxSpeed.TabIndex = 46;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            this.checkBoxSpeed.CheckedChanged += new System.EventHandler(this.checkBoxSpeed_CheckedChanged);
            // 
            // checkBoxHR
            // 
            this.checkBoxHR.AutoSize = true;
            this.checkBoxHR.Location = new System.Drawing.Point(47, 12);
            this.checkBoxHR.Name = "checkBoxHR";
            this.checkBoxHR.Size = new System.Drawing.Size(78, 17);
            this.checkBoxHR.TabIndex = 45;
            this.checkBoxHR.Text = "Heart Rate";
            this.checkBoxHR.UseVisualStyleBackColor = true;
            this.checkBoxHR.CheckedChanged += new System.EventHandler(this.checkBoxHR_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Hide: ";
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(6, 58);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(710, 228);
            this.zedGraphControl1.TabIndex = 43;
            // 
            // radioKm
            // 
            this.radioKm.AutoSize = true;
            this.radioKm.Location = new System.Drawing.Point(34, 546);
            this.radioKm.Name = "radioKm";
            this.radioKm.Size = new System.Drawing.Size(73, 17);
            this.radioKm.TabIndex = 41;
            this.radioKm.TabStop = true;
            this.radioKm.Text = "Kilometres";
            this.radioKm.UseVisualStyleBackColor = true;
            this.radioKm.CheckedChanged += new System.EventHandler(this.radioKm_CheckedChanged);
            // 
            // radioMiles
            // 
            this.radioMiles.AutoSize = true;
            this.radioMiles.Location = new System.Drawing.Point(114, 546);
            this.radioMiles.Name = "radioMiles";
            this.radioMiles.Size = new System.Drawing.Size(49, 17);
            this.radioMiles.TabIndex = 42;
            this.radioMiles.TabStop = true;
            this.radioMiles.Text = "Miles";
            this.radioMiles.UseVisualStyleBackColor = true;
            this.radioMiles.CheckedChanged += new System.EventHandler(this.radioMph_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 576);
            this.Controls.Add(this.radioMiles);
            this.Controls.Add(this.radioKm);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblMaxAltitude);
            this.Controls.Add(this.lblAvAltitude);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.lblMaxPower);
            this.Controls.Add(this.lblAvPower);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.lblMinHR);
            this.Controls.Add(this.lblMaxHeartRate);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lblAvHR);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblMaxSpeed);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblAvSpeed);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblTotalDist);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblVO2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblRestHR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblMaxHR);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblStartTime);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblLength);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.CycleDataGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPageGraph.ResumeLayout(false);
            this.tabPageGraph.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView CycleDataGrid;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMaxHR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblRestHR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblVO2;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTotalDist;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblAvSpeed;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblMaxSpeed;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblAvHR;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblMaxHeartRate;
        private System.Windows.Forms.Label lblMinHR;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblAvPower;
        private System.Windows.Forms.Label lblMaxPower;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblAvAltitude;
        private System.Windows.Forms.Label lblMaxAltitude;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPageGraph;
        private System.Windows.Forms.RadioButton radioKm;
        private System.Windows.Forms.RadioButton radioMiles;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.CheckBox checkBoxPower;
        private System.Windows.Forms.CheckBox checkBoxAltitude;
        private System.Windows.Forms.CheckBox checkBoxCadence;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.CheckBox checkBoxHR;
        private System.Windows.Forms.Label label11;
    }
}

