﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            string file_name = "F:\\ASDBExampleCycleComputerData.hrm";
            string textLine = "";

            if (System.IO.File.Exists(file_name) == true)
            {
                System.IO.StreamReader objReader;
                objReader = new System.IO.StreamReader(file_name);

                do
                {
                    textLine = textLine + objReader.ReadLine() + "\r\n"; //ReadLine method grabs one line from text file. grabs all the lines one at a time
                } while (objReader.Peek() != -1); //peek method checks one char at a time. returns -1 if odesnt find char.

                objReader.Close();
            }
            else
            {
                MessageBox.Show("No Such File" + file_name);
            }

            textBox1.Text = textLine;
        }
    }
}
